# ComboTools V2

ComboTools V2 is an combo cleaner written in C++

## Goal

ComboTools goals are being fast, reliable and easy to use. \
If your operating system is able to run QT, then u can run ComboTools.

## Features

### Main Window

- Load Combos
- Sort Combos
- Remove Duplicate Combos
- email:pass to user:pass
- user:pass to email:pass

### Settings

- Change Theme

### Planned Features

- InBuild Checker? (Or make them standalone, but connect them with ComboTools via socket. Then we could make the protocol part of LibCombo and everyone can write checker that are able to talk with ComboTools. )

- Combo Scraper (Funny web scraping action to make life easier)

## Build ComboTools

### Windows

1. edit the CMakeLists.txt and change the QT Prefix
2. compile :)

### \*nix

```
git submodule init
git submodule update
./scripts/init_cmake.sh Release -O2 -Wall -Werror
cd build
make -j4
```
