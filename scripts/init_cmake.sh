#!/bin/sh -e

if [ $# -lt 1 ]
then
  echo "Syntax: ./scripts/init_cmake.sh <Release | Debug> <CFLAGS>"
  exit 1
fi

TYPE="$1"
shift 2
FLAGS="$*"

rm -rf build/
mkdir build/
(
	cd build/
	cmake .. -DCMAKE_EXPORT_COMPILE_COMMANDS=1 \
	 	 -DCMAKE_CXX_FLAGS="$FLAGS" \
		 -DCMAKE_BUILD_TYPE="$TYPE"
)
