#ifndef COMBOTOOLS_SETTINGS_HPP
#define COMBOTOOLS_SETTINGS_HPP

#include <QApplication>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QObject>
#include <QWidget>

#include <cstdint>
#include <exception>


namespace CT {
enum struct THEME : uint8_t { DARK, NORDIC, PINK, SIEH };

class Settings : public QObject {
  Q_OBJECT
public:
  static Settings &getInstance() {
    static Settings instance;
    return instance;
  }

  Settings(Settings const &) = delete;
  void operator=(Settings const &) = delete;

  void load(const QString &path);
  void save(const QString &path);

  THEME getTheme() const;

  void setTheme(THEME theme);

private:
  Settings() : QObject(nullptr) {}

  THEME theme = THEME::DARK;
  QString saved_token = "";
};
} // namespace CT

#endif // COMBOTOOLS_SETTINGS_HPP
