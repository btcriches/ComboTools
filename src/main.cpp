#include <QApplication>

#include "MainWindow.hpp"
#include "Settings.hpp"

int main(int argc, char *argv[]) {
  QApplication a(argc, argv);

  // load config and init settings system
  auto &settings = CT::Settings::getInstance();
  settings.load("settings.json");

  // set executable icon
  QIcon icon{":/icon.png"};
  QApplication::setWindowIcon(icon);

  new CT::MainWindow(nullptr);
  return QApplication::exec();
}
