#ifndef COMBOTOOLS_MAINWINDOW_HPP
#define COMBOTOOLS_MAINWINDOW_HPP

#include <QFileDialog>
#include <QFrame>
#include <QGridLayout>
#include <QLabel>
#include <QListWidget>
#include <QPushButton>
#include <QString>

#include "Widgets/EmailGenerator.hpp"
#include "Widgets/SideMenu.hpp"
#include "../libcombo/ComboList.hpp"

namespace CT {
class MainWindow : public QFrame {
public:
  explicit MainWindow(QWidget *parent);

private slots:
  void onLoadCombos(const std::basic_string<char, std::char_traits<char>,
                                            std::allocator<char>> &filepath);

  void onSortCombos();

  void onUnique();

  void onEmailToUser();

  void onUserToEmail();

  void onExport();

private:
  void reloadList();

  Widgets::SideMenu *side_menu;
  Widgets::EmailGenerator *email_generator;

  LibCombo::ComboList combo_list;

  QPushButton *load_btn;
  QPushButton *sort_btn;
  QPushButton *unique_btn;
  QPushButton *strip_mail_btn;
  QPushButton *create_mail_btn;
  QPushButton *export_btn;
  QListWidget *combo_list_widget;
};
} // namespace CT

#endif // COMBOTOOLS_MAINWINDOW_HPP
