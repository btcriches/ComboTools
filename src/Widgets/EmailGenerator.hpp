#ifndef COMBOTOOLS_EMAILGENERATOR_HPP
#define COMBOTOOLS_EMAILGENERATOR_HPP

#include <QCheckBox>
#include <QFrame>
#include <QGridLayout>
#include <QPushButton>
#include <QTextEdit>
#include <QVector>

#include "../../libcombo/ComboList.hpp"

namespace CT::Widgets {
class EmailGenerator : public QFrame {
public:
  EmailGenerator(LibCombo::ComboList &combo_List, QWidget *parent);

private:
  QTextEdit *input;
  QCheckBox *delete_user_cbox;
  QPushButton *apply_btn;
};
} // namespace CT::Widgets

#endif // COMBOTOOLS_EMAILGENERATOR_HPP
