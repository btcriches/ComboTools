#include "EmailGenerator.hpp"

#include <iostream>

namespace CT::Widgets {
EmailGenerator::EmailGenerator(LibCombo::ComboList &combo_list, QWidget *parent)
    : QFrame(parent),
      input(new QTextEdit("gmail.com<br>outlook.com<br>gmx.net", this)),
      delete_user_cbox(new QCheckBox("Delete User Combos", this)),
      apply_btn(new QPushButton("Apply", this)) {
  this->setWindowFlag(Qt::WindowType::Window, true);
  this->setAttribute(Qt::WA_DeleteOnClose, true);

  this->setWindowTitle("Setup User to Email");

  auto *layout = new QGridLayout(this);

  input->setToolTip("Email domains are seperated by newlines.\nThe @ is "
                    "automatically added!");
  input->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
  layout->addWidget(input, 0, 0);

  layout->addWidget(delete_user_cbox, 1, 0, Qt::AlignCenter);

  layout->addWidget(apply_btn, 2, 0);

  // apply button connect
  connect(apply_btn, &QPushButton::clicked, this, [&]() {
    if (input->toPlainText().isEmpty()) {
      return;
    }

    // I hate QT for trying to implement their own shit and break or slow down
    // everything that's not using QT types. I have to convert so much bullshit,
    // because I want to keep libcombo usable for programs that dosn't use QT.
    // ;-; <- angry me
    const auto mails = input->toPlainText().split("\n");
    std::vector<std::string> vec;

    for (const auto &str : mails) {
      if (str.isEmpty()) {
        continue;
      }
      vec.push_back(str.toStdString());
    }

    combo_list.userToEmail(vec, this->delete_user_cbox->isChecked());
    this->close();
  });

  this->setLayout(layout);
  this->show();
}

} // namespace CT::Widgets
