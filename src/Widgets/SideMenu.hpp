#ifndef COMBOTOOLS_SIDEMENU_HPP
#define COMBOTOOLS_SIDEMENU_HPP

#include <QPushButton>
#include <QVBoxLayout>
#include <QWidget>

#include "About.hpp"
#include "SettingsWindow.hpp"

namespace CT::Widgets {
class SideMenu : public QWidget {
public:
  explicit SideMenu(QWidget *parent);

private:
  SettingsWindow *settings_win;

  QPushButton *about_btn;
  QPushButton *settings_btn;
};

} // namespace CT::Widgets

#endif // COMBOTOOLS_SIDEMENU_HPP
