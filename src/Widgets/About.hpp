#ifndef COMBOTOOLS_ABOUT_HPP
#define COMBOTOOLS_ABOUT_HPP

#include <QDesktopServices>
#include <QFrame>
#include <QLabel>
#include <QUrl>
#include <QVBoxLayout>

namespace CT::Widgets {
class About : public QFrame {
public:
  explicit About(QWidget *parent);
};
} // namespace CT::Widgets

#endif // COMBOTOOLS_ABOUT_HPP
