#include "SettingsWindow.hpp"

namespace CT::Widgets {
SettingsWindow::SettingsWindow(QWidget *parent)
    : QFrame(parent), combo_box(new QComboBox(this)) {
  this->setWindowFlag(Qt::WindowType::Window, true);
  this->setAttribute(Qt::WA_DeleteOnClose, true);

  this->setWindowTitle("Settings");

  auto *layout = new QGridLayout(this);
  layout->setAlignment(Qt::AlignTop);

  auto *theme_label = new QLabel("Theme", this);
  layout->addWidget(theme_label, 0, 0);

  combo_box->addItems({"Dark", "Nordic", "Pink", "SoldierIstEinHurensohn"});
  combo_box->setCurrentIndex(static_cast<uint8_t>(Settings::getInstance().getTheme()));
  layout->addWidget(combo_box, 0, 1);

  connect(combo_box, qOverload<int>(&QComboBox::currentIndexChanged), this,
          &SettingsWindow::onThemeChanged);

  this->setLayout(layout);
  this->show();
}

void SettingsWindow::onThemeChanged(int index) {
  auto &settings = Settings::getInstance();
  settings.setTheme(static_cast<THEME>(index));
  settings.save("settings.json");
}
} // namespace CT::Widgets
