#include "About.hpp"

namespace CT::Widgets {
About::About(QWidget *parent) : QFrame(parent) {
  this->setWindowFlag(Qt::WindowType::Window, true);
  this->setAttribute(Qt::WA_DeleteOnClose, true);

  this->setFixedSize(400, 200);

  this->setWindowTitle("ComboTools V2 by qwq");

  auto *layout = new QVBoxLayout(this);

  auto *label1 = new QLabel("ComboTools V2", this);
  label1->setObjectName("AboutLabel1");
  layout->addWidget(label1, 0, Qt::AlignCenter);

  auto *label2 = new QLabel("by qwq <a "
                            "href=\"https://cracking.org/members/qwq.407748/\" "
                            "style=\"color: #DEDEDE;\">(cracking.org)</a>");
  label2->setObjectName("AboutLabel2");
  label2->setTextInteractionFlags(Qt::TextBrowserInteraction);
  layout->addWidget(label2, 0, Qt::AlignCenter);

  connect(label2, &QLabel::linkActivated, this,
          [](const QString &link) { QDesktopServices::openUrl(QUrl(link)); });

  this->setLayout(layout);
  this->show();
}
} // namespace CT::Widgets