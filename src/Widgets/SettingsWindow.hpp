#ifndef COMBOTOOLS_SETTINGSWINDOW_HPP
#define COMBOTOOLS_SETTINGSWINDOW_HPP

#include <QComboBox>
#include <QFrame>
#include <QGridLayout>
#include <QLabel>
#include <QApplication>

#include "../Settings.hpp"

namespace CT::Widgets {
class SettingsWindow : public QFrame {
public:
  explicit SettingsWindow(QWidget *parent);

public slots:
  void onThemeChanged(int index);

private:
  QComboBox *combo_box;
};
} // namespace CT::Widgets

#endif // COMBOTOOLS_SETTINGSWINDOW_HPP
