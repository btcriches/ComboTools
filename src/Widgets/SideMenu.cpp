#include "SideMenu.hpp"

namespace CT::Widgets {
SideMenu::SideMenu(QWidget *parent)
    : QWidget(parent), settings_win(nullptr),
      about_btn(new QPushButton("About", this)),
      settings_btn(new QPushButton("Settings", this)) {
  auto *layout = new QVBoxLayout(this);
  layout->setAlignment(Qt::AlignTop);

  layout->addWidget(about_btn);
  layout->addWidget(settings_btn);

  connect(about_btn, &QPushButton::clicked, this,
          [this]() { new About(this); });

  connect(settings_btn, &QPushButton::clicked, this, [this]() {
    if (settings_win == nullptr) {
      settings_win = new SettingsWindow(this);

      connect(settings_win, &QFrame::destroyed, this, [&]() {
        settings_win = nullptr;
      });
    }


  });

  this->setLayout(layout);
}
} // namespace CT::Widgets