#include "Settings.hpp"
#include <stdexcept>

namespace CT {
void Settings::load(const QString &path) {
  auto file = QFile{"settings.json"};
  if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
    this->setTheme(THEME::DARK);
    return; // use default values
  }

  QJsonParseError parse_error{};

  auto json_doc = QJsonDocument::fromJson(file.readAll(), &parse_error);
  if (parse_error.error != QJsonParseError::NoError) {
    return; // also use default values
  }

  auto obj = json_doc.object();
  this->setTheme(static_cast<THEME>(obj["Theme"].toInt()));
  file.close();
}

void Settings::save(const QString &path) {
  auto file = QFile{"settings.json"};
  if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
    throw std::logic_error("Failed to open settings file");
  }

  QJsonObject obj_global;
  obj_global.insert("Theme", static_cast<uint16_t>(this->theme));

  QJsonDocument json_doc{obj_global};

  file.write(json_doc.toJson());
  file.close();
}

THEME Settings::getTheme() const { return this->theme; }

void Settings::setTheme(THEME theme) {
  this->theme = theme;

  QFile *file;
  switch (theme) {
  case CT::THEME::DARK:
    file = new QFile{":/dark.qss"};
    break;
  case CT::THEME::NORDIC:
    file = new QFile{":/nordic.qss"};
    break;
  case CT::THEME::PINK:
    file = new QFile{":/pink.qss"};
    break;
  case CT::THEME::SIEH:
    file = new QFile{":/SoldierIstEinHurensohn.qss"};
    break;
  default:
    return;
  }

  file->open(QFile::ReadOnly);
  dynamic_cast<QApplication *>(QApplication::instance())
      ->setStyleSheet((file->readAll()));
  file->close();
  delete file;
}
} // namespace CT
