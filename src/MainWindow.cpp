#include "MainWindow.hpp"

#include <iostream>

namespace CT {
MainWindow::MainWindow(QWidget *parent)
    : QFrame(parent),side_menu(new Widgets::SideMenu(this)),
      email_generator(nullptr), combo_list(),
      load_btn(new QPushButton("Load File", this)),
      sort_btn(new QPushButton("Sort", this)),
      unique_btn(new QPushButton("Remove Duplicates", this)),
      strip_mail_btn(new QPushButton("Email to User", this)),
      create_mail_btn(new QPushButton("User to Email", this)),
      export_btn(new QPushButton("Export", this)),
      combo_list_widget(new QListWidget(this)) {
  this->setWindowFlag(Qt::WindowType::Window, true);
  this->setAttribute(Qt::WA_DeleteOnClose, true);

  this->setWindowTitle("ComboTools V2");

  auto *layout = new QGridLayout{this};

  layout->addWidget(side_menu, 1, 0, 2, 1);

  layout->addWidget(load_btn, 0, 1);

  layout->addWidget(sort_btn, 0, 2);

  layout->addWidget(unique_btn, 0, 3);

  layout->addWidget(strip_mail_btn, 0, 4);

  layout->addWidget(create_mail_btn, 0, 5);

  layout->addWidget(export_btn, 0, 6);

  combo_list_widget->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
  layout->addWidget(combo_list_widget, 1, 1, 2, 6);

  // load connect
  connect(load_btn, &QPushButton::clicked, this, [&]() {
    const auto &names = QFileDialog::getOpenFileNames(this, "Load ComboList");
    if (names.isEmpty()) {
      return;
    }

    for (const auto &path : names) {
      if (path.isEmpty()) {
        return;
      }

      this->onLoadCombos(path.toStdString());
    }
  });

  // sort connect
  connect(sort_btn, &QPushButton::clicked, this, &MainWindow::onSortCombos);

  // unique connect
  connect(unique_btn, &QPushButton::clicked, this, &MainWindow::onUnique);

  // email2user connect
  connect(strip_mail_btn, &QPushButton::clicked, this,
          &MainWindow::onEmailToUser);

  // user2email connect
  connect(create_mail_btn, &QPushButton::clicked, this,
          &MainWindow::onUserToEmail);

  // export connect
  connect(export_btn, &QPushButton::clicked, this, &MainWindow::onExport);

  this->setLayout(layout);
  this->show();
}

void MainWindow::onLoadCombos(
    const std::basic_string<char, std::char_traits<char>, std::allocator<char>>
        &filepath) {
  this->combo_list.extend(filepath);

  size_t widget_size = combo_list_widget->count();
  const size_t combo_list_size = this->combo_list.size();
  if (widget_size > combo_list_size) {
    throw std::logic_error("Error: Widget size is greater than the ComboList "
                           "size???? I am confused qwq????");
  }

  while (widget_size != combo_list_size) {
    combo_list_widget->insertItem(
        widget_size,
        new QListWidgetItem(
            this->combo_list.at(widget_size).getComboStr().c_str()));
    widget_size++;
  }
}

void MainWindow::onSortCombos() {
  this->combo_list.sort();
  this->reloadList();
}

void MainWindow::onUnique() {
  this->combo_list.deldup();
  this->reloadList();
}

void MainWindow::onEmailToUser() {
  this->combo_list.emailToUser();
  this->reloadList();
}

void MainWindow::onUserToEmail() {
  if (email_generator == nullptr) {
    email_generator = new Widgets::EmailGenerator(this->combo_list, this);

    connect(email_generator, &QFrame::destroyed, this, [&]() {
      email_generator = nullptr;
      this->reloadList();
    });
  }
}

void MainWindow::onExport() {
  const auto &path = QFileDialog::getSaveFileName(this, "Save ComboList");
  if (path.isEmpty()) {
    return;
  }

  this->combo_list.storeToHarddisk(path.toStdString());
}

void MainWindow::reloadList() {
  this->combo_list_widget->clear();
  for (const auto &combo : this->combo_list) {
    combo_list_widget->addItem(combo.getComboStr().c_str());
  }
}

} // namespace CT
